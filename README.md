# InfoJS

A Simple tool to view your browser information and your fingerprint.

## Mentions

These are easily spoofed and are only used to display to the user and not to create the actual fingerprint.

- UserAgent
- Canvas ( Ignored if the second cycle does not match )
- Hardware Concurrency

## Factors

- Languages
- Locales
- Time Zones
- Screen Resolution
- Touch Support and Max Number of Touch 
- Brave from other Chromium-based
- Firefox from others
- Java ( useless )
- Media Constraints ( Echo Cancelation, Scroll with Page... )
- RAM
- Product Sub