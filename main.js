let worker = new Worker('./worker.js'),
    cfp1 = getCanvas(),
    cfp2 = getCanvas(),
    cfp = cfp1 == cfp2 ? cfp1 : '',
    res = `${screen.availWidth * window.devicePixelRatio}x${screen.availHeight * window.devicePixelRatio}-${screen.pixelDepth}-${screen.colorDepth}`,
    rec = {
      res: res,
      canvas: cfp,
      brave: !!navigator.brave,
      moz: !!navigator.mozGetUserMedia,
      cookie: navigator.cookieEnabled,
      java: navigator.javaEnabled(),
      touch: navigator.maxTouchPoints,
      id: navigator.buildID || 0,
      m_const: navigator.mediaDevices.getSupportedConstraints(),
      priv: navigator.globalPrivacyControl,
      style: getStyles(),
      script: getScripts()
    };

worker.postMessage(rec);

worker.onmessage = data => {
  
  let [hash, info] = data.data;
  console.log(hash, info);

  // TODO: Show all Info used in the Fingerprint
  id('hash').innerHTML = hash;
  id('st').innerHTML = info.date.st;
  id('locale').innerHTML = info.date.locale;
  id('tz').innerHTML = info.date.tz + ', ' + info.date.tzOff;
  id('default').innerHTML = info.language.default;
  id('avail').innerHTML = info.language.available.join(', ');
  id('sys').innerHTML = navigator.userAgent.match(/\((.*?)\)/)[1].replaceAll(';',',');
  id('res').innerHTML = info.sys.resolution.replaceAll('-',' ');
  id('canvas').src = info.misc.canvas && 1 != info.misc.canvas ? info.misc.canvas: cfp1;
  id('cores').innerHTML = navigator.hardwareConcurrency;

}

function getScripts() {
  let arr = [];
  for (let html of document.scripts) {
    let json = {}, vals = ["attributes","classList"];
    for (let val of vals) {
      for (let attr of html[val]) {
        json[attr.name] = attr.value;
      }
      arr.push(json)
    }
  }

  return arr
}

function getStyles() {
  let json = {};

  for (let style of document.styleSheets) {
    json[style.href] = [...style.cssRules].map(rule => rule.cssText).join('')
  }
}
function getCanvas() {
  
  let canvas = document.createElement('canvas'),
      ctx = canvas.getContext('2d'),
      txt = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#$%^&*?~';
    
  ctx.textBaseline = 'top';
  ctx.font = '14px Arial';
  ctx.textBaseline = 'alphabetic';
  ctx.fillStyle = '#ccc';
  ctx.fillRect(125, 1, 62, 20);
  ctx.fillStyle = '#111';
  ctx.fillText(txt, 2, 15);
  ctx.fillStyle = 'rgba( 200, 100, 0, .6)';
  
  return canvas.toDataURL()

};

function id(arg) { return document.getElementById(arg) }