importScripts("//cdn.jsdelvr.net/npm/xxhashjs/build/xxhash.min.js")

onmessage = eve => {

    let data = getData(eve.data),
        hash = XXH.h64(JSON.stringify(data), 0xABCD).toString(16);

    postMessage([hash, data]);

}

function getData(rec) {

    let stStr = new Date().toLocaleDateString(undefined, { timeZoneName: 'long' }).split(',').pop(),
        data = {
            sys: {
                resolution: rec.res,
                isBrave: rec.brave,
                isFfx: rec.moz,
                isChrome: !!self.webkitResolveLocalFileSystemURL || false,
                cookie: rec.cookie,
                touch: rec.touch,
                java: rec.java,
                priv: rec.priv,
                buildId: rec.id,
                mem: rec.brave || navigator.deviceMemory || 0
            },
            language: {
                default: navigator.language,
                available: navigator.languages
            },
            date: {
                st: stStr.substring(1, stStr.length),
                locale: Intl.DateTimeFormat().resolvedOptions().locale,
                tz: Intl.DateTimeFormat().resolvedOptions().timeZone,
                tzOff: new Date().getTimezoneOffset()
            },
            media: {
                constraints: rec.m_const,
                style: rec.style || [],
                scripts: rec.script || []
            },
            misc: {
                canvas: rec.brave || rec.canvas
            }
        };

    return data

}